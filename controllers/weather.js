var weather = require('weather-js')
/**
 * Retrieve a specific weather through location id
 *
 * @method view
 *
 * @param req
 * @param res
 * @param next
 * @returns {Function} next
 */

exports.index = function (req, res, next) {
    weather.find({search: req.params.location, degreeType: 'F'}, function (err, result) {
        if (err) console.log(err);
        res.send(200, result);
    });

}

