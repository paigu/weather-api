module.exports = function( server ){

    // weather
    var weather = require( '../controllers/weather' );
    server.get( '/weather', weather.index );
};